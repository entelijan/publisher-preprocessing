import random
from pathlib import Path
from flair.data import Sentence
from flair.models import SequenceTagger

print("embeddings")


def read() -> list[str]:
    infile = Path("/home/wwagner4/prj/gutenberg/data/publisher/publisher.txt")
    if not infile.exists():
        raise RuntimeError(f"Infile {infile} must exist")

    with infile.open("r") as f:
        lines = f.readlines()
    lines = [l.rstrip() for l in lines]
    lines = set([l.lower() for l in lines])
    return sorted(lines)


ps = read()
random.shuffle(ps)
ps = ps[0:1]


def tagging():
    tagger = SequenceTagger.load("flair/upos-multi")
    # tagger = SequenceTagger.load("flair/ner-german")
    # tagger = SequenceTagger.load("flair/ner-german-large")
    for p in ps:
        sentence = Sentence(p)
        print(sentence)
        tagger.predict(sentence)
        for t in sentence:
            print(t)

import scipy
import word2vec

import nltk



a = "hallo"
b = "hello"
va = word2vec
vb = word2vec.scripts_interface.word2vec(b)
d = scipy.spatial.distance.cosine(va, vb)
print(a, b, d)
